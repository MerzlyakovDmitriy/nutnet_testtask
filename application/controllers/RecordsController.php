<?php

class RecordsController extends Zend_Controller_Action
{

    public function init()
    {
        /* Initialize action controller here */
    }

    public function indexAction()
    {
        $records = new Application_Model_RecordsMapper();
        $paginator = Zend_Paginator::factory($records->fetchAll());
        $paginator->setCurrentPageNumber($this->_getParam('page', 1));
        $this->view->paginator = $paginator;
    }

    public function addAction()
    {
        $request = $this->getRequest();
        $form    = new Application_Form_Records();
        $form->submit->setLabel('Добавить');

        if ($this->getRequest()->isPost()) {
            if ($form->isValid($request->getPost())) {
                $record = new Application_Model_Records($form->getValues());
                $mapper  = new Application_Model_RecordsMapper();;
                $mapper->saveRecord($record);
                return $this->_helper->redirector('index');
            }
        }
        $this->view->form = $form;
    }

    public function editAction()
    {
        $request = $this->getRequest();
        $form = new Application_Form_Records();
        $form->submit->setLabel('Редактировать');

        if ($this->getRequest()->isPost()) {
            if ($form->isValid($request->getPost())) {
                $record = new Application_Model_Records($form->getValues());
                $records = new Application_Model_DbTable_Records();
                $records->updateRecord($record);
                return $this->_helper->redirector('index');
            } else {
                $form->populate($formData);
            }
        } else {
                $id = $this->_getParam('id',0);
                if ($id>0){
                    $records = new Application_Model_DbTable_Records();
                    $form->populate($records->getRecord($id));
                }
        }
        $this->view->form = $form;
    }

    public function deleteAction()
    {
        if ($this->getRequest()->isPost()){
            $del = $this->getRequest()->getPost('del');
            if ($del == 'Да') {
                $id = $this->getRequest()->getPost('id');
                $records = new Application_Model_DbTable_Records();
                $records->deleteRecord($id);
            }
            $this->_helper->redirector('index');
        } else {
            $id = $this->_getParam('id',0);
            $records = new Application_Model_DbTable_Records();
            $this->view->records = $records->getRecord($id);
        }
    }


}







