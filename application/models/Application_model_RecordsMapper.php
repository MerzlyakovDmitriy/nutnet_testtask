<?php

class Application_Model_GuestbookMapper
{
    public function saveRecord($model);
    public function find($id, $model);
    public function fetchAll();
    public function updateRecord($model);
    public function getRecord($id);
}
