<?php

class Application_Model_DbTable_Records extends Zend_Db_Table_Abstract
{

    protected $_name = 'records';

    public function getRecord($id){
        $id = (int)$id;
        $row = $this->fetchRow('id = ' . $id);
        if (!$row){
            throw new Exception("Could not find row $id");
        }
        return $row->toArray();
    }

    public function addRecord($record){
        $this->insert($record);
    }

    public function updateRecord($record){
        $id = $record->id;
        $data = array(
            'artist' => $record->artist,
            'title'  => $record->title,
        );
        $this->update($data, 'id ='. $id);
    }

    public function deleteRecord($id){
        $this->delete('id =' . $id );
    }
}

