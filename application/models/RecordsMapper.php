<?php

class Application_Model_RecordsMapper
{
protected $_dbTable;

    public function setDbTable($dbTable)
    {
        if (is_string($dbTable)) {
            $dbTable = new $dbTable();
        }
        if (!$dbTable instanceof Zend_Db_Table_Abstract) {
            throw new Exception('Invalid table data gateway provided');
        }
        $this->_dbTable = $dbTable;
        return $this;
    }

    public function getDbTable()
    {
        if (null === $this->_dbTable) {
            $this->setDbTable('Application_Model_DbTable_Records');
        }
        return $this->_dbTable;
    }

    public function save(Application_Model_Records $records)
    {
        $data = array(
            'title'   => $records->getTitle(),
            'artist' => $records->getArtist(),
        );

        if (null === ($id = $records->getId())) {
            unset($data['id']);
            $this->getDbTable()->insert($data);
        } else {
            $this->getDbTable()->update($data, array('id = ?' => $id));
        }
    }

    public function find($id, Application_Model_Records $records)
    {
        $result = $this->getDbTable()->find($id);
        if (0 == count($result)) {
            return;
        }
        $row = $result->current();
        $records->setId($row->id)
                  ->setTitle($row->title)
                  ->setArtist($row->artist);
    }

    public function fetchAll()
    {
        $resultSet = $this->getDbTable()->fetchAll();
        $entries   = array();
        foreach ($resultSet as $row){
            $entry = new Application_Model_Records();
            $entry->setId($row->id)
                  ->setTitle($row->title)
                  ->setArtist($row->artist);
            $entries[] = $entry;
        }
        return $entries;
    }

}

