<?php

class Application_Form_Records extends Zend_Form
{
    public function init()
    {
        // Set the method for the display form to POST
        $this->setMethod('post');

        $this->addElement('hidden','id',array(
            'required'  =>true
        ));

        $this->addElement('text', 'title', array(
            'label'      => 'Название:',
            'required'   => true,
            'filters'    => array('StringTrim'),
        ));

        $this->addElement('text', 'artist', array(
            'label'      => 'Исполнитель:',
            'required'   => true,
            'filters'    => array('StringTrim'),
        ));

        $this->addElement('submit', 'submit', array(
            'ignore'   => true,
            'label'    => 'add',
        ));
    }


}

